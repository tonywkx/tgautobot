import {Markup, Telegraf, session} from 'telegraf'
import config from 'config'
import { translationDictionary1, translationDictionary2, translationDictionary3, translationDictionary4 } from './i18n.js';
import { getAutoMarketKeyboard, getBack, getDetailingKeyboard, getFormattedData, getMenuKeyboard, getPartsKeyboard, getRepairTypesKeyboard } from './utils.js';

console.log(config.get('TEST_ENV'))

const bot = new Telegraf(config.get("TELEGRAM_TOKEN"));
bot.use(session());

// Обработчик команды /start
bot.start( async (ctx) => {
  ctx.telegram.sendMessage(ctx.chat.id, `🎉 Добро пожаловать в наш умный Автобот ${ctx.chat.first_name} ! Выберете пункт меню: `, getMenuKeyboard());
});

// Handle the '/help' command
bot.help((ctx) => {
  // Send a help message
  ctx.reply('Пока это простой бот. Вы можете написать /start чтобы начать заново.');
});

// Обработчик выбора пункта меню 1 "Заказать агрегат"
bot.hears('⚙️ Заказать агрегат', (ctx) => {
  ctx.session = {}; // Сброс сессии пользователя
  ctx.session.menuItem = 'Заказ на агрегат'; // Номер пункта меню
  ctx.session.data = {}; // Инициализация объекта данных
  ctx.reply('Осталось 3️⃣ этапа. Введите марку и модель авто:');
});

// Обработчик выбора пункта меню 2 "Ремонт автомобиля"
bot.hears('🔧 Ремонт автомобиля', (ctx) => {
  ctx.reply('Выберите вид ремонта:', getRepairTypesKeyboard());
});

bot.hears('🔨 Слесарный ремонт', (ctx) => {
  const services = [
    'Точка\nДмитрий\n8 929 277 12 34',
    'АКПП Сервис\nАлександр\n8 982 791 00 89',
    'Автосервис ВАЗ\nПавел\n8 950 157 00 58',
    'Тойота сервис\nАлексей\n8 912 450 06 86',
    'Автосервис Автомойкин\nИгорь\n8 905 875 03 95',
  ];

  const formattedServices = services.map(service => {
    const [name, contact, phone] = service.split('\n');
    return `• *${name}* \n ${contact}\n   _${phone}_`;
  }).join('\n'); 

  const message = `Список сервисов:\n${formattedServices}`;

  ctx.replyWithMarkdownV2(message, getBack());
})

bot.hears('⚒ Капитальный ремонт ДВС', (ctx) => {
  ctx.reply('Здесь пока нет контактов. Вы можете стать партнером, обращайтесь', getBack());
})

bot.hears('💥 Кузовной ремонт', (ctx) => {
  ctx.reply('Ангар Дмитрий +7 912 853 77 00', getBack());
});

// Обработчик выбора пункта меню 3 "Заказать запчасти"
bot.hears('🔩 Заказать запчасти', (ctx) => {
  ctx.reply('Выберите новые или б/у:', getPartsKeyboard());
});

bot.hears('🚄 Новые', (ctx) => {
  ctx.session = {}; 
  ctx.session.menuItem = 'Заказ на новые запчасти'; 
  ctx.session.data = {}; 
  ctx.reply('Осталось 4️⃣ этапа. Введите марку и модель авто:');
});

bot.hears('🚂 Б/У', (ctx) => {
  ctx.session = {}; 
  ctx.session.menuItem = 'Заказ на Б/У запчасти'; 
  ctx.session.data = {}; 
  ctx.reply('Осталось 4️⃣ этапа. Введите марку и модель авто:');
});

// Обработчик выбора пункта меню 4 "Авторынок"

bot.hears('🚗 Авторынок', (ctx) => {
  ctx.reply('Выберите что вас интересует:', getAutoMarketKeyboard());
});

bot.hears('📒 Подбор автомобиля', (ctx) => {
  ctx.session = {}; 
  ctx.session.menuItem = 'Заказ на подбор автомобиля'; 
  ctx.session.data = {}; 
  ctx.reply('Отлично, напишите предполагаемый бюджет на покупку', getBack());
})

bot.hears('🔎 Выездная диагностика автомобиля/эксперт на день', (ctx) => {
  ctx.session = {}; 
  ctx.session.menuItem = 'Заказ на выездную диагностику'; 
  ctx.session.data = {}; 
  ctx.reply('Сколько машин нужно посмотреть?', getBack());
})

bot.hears('💰 Выкуп автомобиля', (ctx) => {
  ctx.session = {}; 
  ctx.session.menuItem = 'Заказ на выкуп автомобиля'; 
  ctx.session.data = {}; 
  ctx.reply('Осталось 7 этапов. Введите марку авто:', getBack());
});

// Обработчик выбора пункта меню 5 "Детейлинг"
bot.hears('🚿 Детейлинг', (ctx) => {
  ctx.reply('Выберите вид детейлинга:', getDetailingKeyboard());
});

bot.hears('🔮 Полировка автомобиля', (ctx) => {
  const serviceName = 'Полировка18 Евгений';
  const userLink = ctx.message.from ? ctx.message.from.username : 'Пользователь не найден';
  let readyLink = `@${userLink}`
  const message = `Пользователь ${readyLink} запросил информацию о сервисе:\n${serviceName}`;
  
  // Отправляем сообщение в чат с менеджером
  bot.telegram.sendMessage('@autobotizhevsk', message); // Замените 'MANAGER_CHAT_ID' на ID чата с менеджером
  ctx.reply('Полировка18 Евгений +7 912 020 97 83', getBack());
})

bot.hears('🔬 Химчистка салона', (ctx) => {
  ctx.reply('Автомойкин химчистка Александра +7 982 122 76 85', getBack());
})

bot.hears('🔑 Комплекс работ', (ctx) => {
  ctx.reply('Здесь пока нет контактов. Вы можете стать партнером, обращайтесь', getBack());
});

// Обработчик выбора пункта меню 6 "Автоюрист"
bot.hears('⚖️ Автоюрист', (ctx) => {
  ctx.reply('Юрист Николай: +7 912 454 31 95',getBack());
});
// Обработчик выбора пункта меню 7 "Страховка"
bot.hears('⚔️ Страховка', (ctx) => {
  ctx.reply('ОСАГО, КАСКО: Георгий +7 912 444 99 69',getBack());
});

bot.hears('⬅️ Назад в главное меню', (ctx) => {
  ctx.session = {}; 
  ctx.reply('Добро пожаловать! Выберите пункт меню:', getMenuKeyboard());
})


// Обработчик полученных данных
bot.on('text', (ctx) => {
  const data = ctx.message.text;
  const menuItem = ctx.session.menuItem;
  const sessionData = ctx.session.data;
  const step = ctx.session.step || 1; // Текущий шаг сбора данных
 
  if( menuItem === 'Заказ на агрегат'){
    switch (step) {
      case 1:
        sessionData.zayavka = menuItem
        sessionData.marka = data;
        ctx.session.step = 2;
        ctx.reply('Осталось 2️⃣  этапа. Введите год выпуска и поколение авто:', getBack());
        break;
      case 2:
        sessionData.generation = data;
        ctx.session.step = 3;
        ctx.reply('Осталось 1️⃣ этапа. Что хотите заказать?', getBack());
        break;
      case 3:
        sessionData.orderType = data;
        ctx.session.step = 4;
        ctx.reply('Последний этап. Введите имя и номер телефона:', getBack());
        break;
      case 4:
        sessionData.contactInfo = data;
        const userLink = ctx.message.from ? ctx.message.from.username : 'Пользователь не найден';
        let readyLink = `@${userLink}`
        sessionData.user = readyLink
        let str = getFormattedData(sessionData, translationDictionary1)
        // Отправляем собранные данные менеджеру (здесь указать ID чата менеджера)
        bot.telegram.sendMessage('@autobotizhevsk', str);
        ctx.session = {}; // Сбрасываем сессию пользователя
        ctx.reply('✅ Спасибо за обращение! Менеджер свяжется с вами в ближайшее время.', getBack());
        break;
      default:
        ctx.reply('Что-то пошло не так. Пожалуйста, начните сначала.', getBack());
        ctx.session = {}; // Сбрасываем сессию пользователя
        break;
    }
  }

  if( menuItem === 'Заказ на подбор автомобиля'){
    switch (step) {
      case 1:
        sessionData.zayavka = menuItem
        sessionData.budjet = data;
        ctx.session.step = 2;
        ctx.reply('Осталось 2️⃣ этапов. Напишите ваши пожелания:', getBack());
        break;
      case 2:
        sessionData.comment = data;
        ctx.session.step = 3;
        ctx.reply('Последний этап. Введите имя и номер телефона:', getBack());
        break;
      case 3:
        sessionData.contactInfo = data;
        const userLink = ctx.message.from ? ctx.message.from.username : 'Пользователь не найден';
        let readyLink = `@${userLink}`
        sessionData.user = readyLink
        let str = getFormattedData(sessionData, translationDictionary2)
        // Отправляем собранные данные менеджеру (здесь указать ID чата менеджера)
        bot.telegram.sendMessage('@autobotizhevsk', str);
        ctx.session = {}; // Сбрасываем сессию пользователя
        ctx.reply('✅ Спасибо за обращение! Менеджер свяжется с вами в ближайшее время.', getBack());
        break;
      default:
        ctx.reply('Что-то пошло не так. Пожалуйста, начните сначала.', getBack());
        ctx.session = {}; // Сбрасываем сессию пользователя
        break;
    }
  }

  if( menuItem === 'Заказ на выездную диагностику'){
    switch (step) {
      case 1:
        sessionData.zayavka = menuItem
        sessionData.quantity = data;
        ctx.session.step = 2;
        ctx.reply('Отлично, введите имя и номер телефона:', getBack());
        break;
      case 2:
        sessionData.contactInfo = data;
        const userLink = ctx.message.from ? ctx.message.from.username : 'Пользователь не найден';
        let readyLink = `@${userLink}`
        sessionData.user = readyLink
        let str = getFormattedData(sessionData, translationDictionary3)
        // Отправляем собранные данные менеджеру (здесь указать ID чата менеджера)
        bot.telegram.sendMessage('@autobotizhevsk', str);
        ctx.session = {}; // Сбрасываем сессию пользователя
        ctx.reply('✅ Спасибо за обращение! Менеджер свяжется с вами в ближайшее время.', getBack());
        break;
      default:
        ctx.reply('Что-то пошло не так. Пожалуйста, начните сначала.', getBack());
        ctx.session = {}; // Сбрасываем сессию пользователя
        break;
    }
  }

  if( menuItem === 'Заказ на выкуп автомобиля'){
    switch (step) {
      case 1:
        sessionData.zayavka = menuItem
        sessionData.marka = data;
        ctx.session.step = 2;
        ctx.reply('Осталось 6 этапов. Введите модель авто:', getBack());
        break;
      case 2:
        sessionData.model = data;
        ctx.session.step = 3;
        ctx.reply('Осталось 5 этапа. Введите год выпуска:', getBack());
        break;
      case 3:
        sessionData.year = data;
        ctx.session.step = 4;
        ctx.reply('Осталось 4 этапа. Введите VIN:', getBack());
        break;
      case 4:
        sessionData.vin = data;
        ctx.session.step = 5;
        ctx.reply('Осталось 3 этапа. Введите количество владельцев:', getBack());
        break;
      case 5:
        sessionData.owners = data;
        ctx.session.step = 6;
        ctx.reply('Осталось 2 этапа. Напишите, были ли ДТП:', getBack());
        break;
      case 6:
        sessionData.dtp = data;
        ctx.session.step = 7;
        ctx.reply('Остался 1 этапа. Введите имя и номер телефона:', getBack());
        break;
      case 7:
        sessionData.contactInfo = data;
        ctx.session.step = 8;
        ctx.reply('Последний этап. Загрузите фото автомобиля:', getBack());
        break;
      default:
        ctx.reply('Что-то пошло не так. Пожалуйста, начните сначала.', getBack());
        ctx.session = {}; // Сбрасываем сессию пользователя
        break;
    }
  }

  if(menuItem === 'Заказ на новые запчасти' || menuItem === 'Заказ на Б/У запчасти'){
    switch (step) {
      case 1:
        sessionData.zayavka = menuItem
        sessionData.marka = data;
        ctx.session.step = 2;
        ctx.reply('Осталось 3️⃣ этапов. Введите год выпуска и поколение автомобиля:', getBack());
        break;
      case 2:
        sessionData.generation = data;
        ctx.session.step = 3;
        ctx.reply('Осталось 2️⃣ этапа. Кратко опишите что нужно:', getBack());
        break;
      case 3:
        sessionData.orderType = data;
        ctx.session.step = 4;
        ctx.reply('Осталось 1️⃣ этапа. Напишите имя и номер телефона', getBack());
        break;
      case 4:
        sessionData.contactInfo = data;
        ctx.session.step = 5;
        ctx.reply('Последний этап. Загрузите фото VIN:', getBack());
        break;
      default:
        ctx.reply('Что-то пошло не так. Пожалуйста, начните сначала.', getBack());
        ctx.session = {}; // Сбрасываем сессию пользователя
        break;
    }
  }
});

bot.on('photo', (ctx) => {
  const sessionData = ctx.session.data;
  if(!sessionData){
    ctx.reply('Пожалуйста, начните вводить данные заново', getBack())
    return
  }
  const userLink = ctx.message.from ? ctx.message.from.username : 'Пользователь не найден';
  let readyLink = `@${userLink}`
  sessionData.user = readyLink
  const photo = ctx.message.photo[ctx.message.photo.length - 1];
  const photoId = photo.file_id;
  
  let str;
  if(sessionData.zayavka === 'Заказ на выкуп автомобиля'){
    str = getFormattedData(sessionData, translationDictionary4)
  } else {
    str = getFormattedData(sessionData, translationDictionary1)
  }
  
  bot.telegram.sendPhoto('@autobotizhevsk', photoId, {
    caption: str
  });
  ctx.session = {}; // Сбрасываем сессию пользователя
  ctx.reply('✅ Спасибо за обращение! Менеджер свяжется с вами в ближайшее время.', getBack());
})

bot.launch();

// Enable graceful stop
process.once('SIGINT', () => bot.stop('SIGINT'));
process.once('SIGTERM', () => bot.stop('SIGTERM'));