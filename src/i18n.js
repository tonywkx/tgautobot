export const translationDictionary1 = {
    zayavka: 'Заявка',
    marka: 'Марка и модель',
    model: 'Модель ',
    generation: 'Поколение и год выпуска',
    orderType: 'Тип заказа',
    contactInfo: 'Контактная информация',
    user: 'Ссылка на ТГ',
  };

  export const translationDictionary2 = {
    zayavka: 'Заявка',
    budjet: 'Бюджет',
    comment: 'Пожелания',
    contactInfo: 'Контактная информация',
    user: 'Ссылка на ТГ',
  };

  export const translationDictionary3 = {
    zayavka: 'Заявка',
    quantity: 'Количество машин',
    contactInfo: 'Контактная информация',
    user: 'Ссылка на ТГ',
  };

  export const translationDictionary4 = {
    zayavka: 'Заявка',
    marka: 'Марка',
    model: 'Модель',
    year: 'Год выпуска',
    vin: 'ВИН-код',
    owners: 'Количество владельцев',
    dtp: 'Наличие ДТП',
    contactInfo: 'Контактная информация',
    user: 'Ссылка на ТГ',
  };
