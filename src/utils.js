import {Markup} from 'telegraf'
export const getFormattedData = (data, translationDictionary) => {
    const formattedData = Object.entries(data)
      .map(([key, value]) => {
        const translatedKey = translationDictionary[key] || key; // Используем перевод или оригинальный ключ
        return `${translatedKey}: ${value}`;
      })
      .join('\n');
  
    return formattedData;
}


// Вспомогательная функция для получения клавиатуры главного меню
export function getMenuKeyboard() {
    return Markup.keyboard(['⚙️ Заказать агрегат', '🔧 Ремонт автомобиля', '🔩 Заказать запчасти', '🚗 Авторынок', '🚿 Детейлинг', '⚖️ Автоюрист', '⚔️ Страховка']).resize();
}

export function getRepairTypesKeyboard() {
    return Markup.keyboard(['🔨 Слесарный ремонт', '⚒ Капитальный ремонт ДВС', '💥 Кузовной ремонт', '⬅️ Назад в главное меню']).resize();
}

export  function getDetailingKeyboard() {
    return Markup.keyboard(['🔮 Полировка автомобиля', '🔬 Химчистка салона', '🔑 Комплекс работ', '⬅️ Назад в главное меню']).resize();
}

export function getPartsKeyboard() {
    return Markup.keyboard(['🚄 Новые', '🚂 Б/У', '⬅️ Назад в главное меню']).resize();
}

export function getAutoMarketKeyboard() {
    return Markup.keyboard(['📒 Подбор автомобиля', '🔎 Выездная диагностика автомобиля/эксперт на день', '💰 Выкуп автомобиля', '⬅️ Назад в главное меню']).resize();
}

export function getBack() {
    return Markup.keyboard(['⬅️ Назад в главное меню']).resize();
}